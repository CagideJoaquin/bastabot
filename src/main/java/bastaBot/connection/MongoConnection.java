package bastaBot.connection;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

public class MongoConnection {

    private static MongoConnection instancia;

    private MongoClient mongoClient;
    private MongoDatabase database;

    public MongoConnection() {

        MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
        //build the connection options
        builder.maxConnectionIdleTime(60000);//set the max wait time in (ms)
        MongoClientOptions opts = builder.build();

        try {
            MongoClientURI uri = new MongoClientURI(
                    "mongodb+srv://sabrinasacz:lMdjuMnmaySDbxu9@bastaproject-vo5a5.gcp.mongodb.net/test?retryWrites=true&w=majority&ssl=true&maxIdleTimeMS=5000");

            this.mongoClient = new MongoClient(uri);
            this.database = mongoClient.getDatabase("bastaproject");
            System.out.println("Conexion exitosa");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Conexion fallida");
        }
    }

    public static MongoConnection getConnection() {
        if (instancia == null) {
            instancia = new MongoConnection();
        }
        return instancia;
    }

    public MongoDatabase getDatabase() {
        return database;
    }

    public void cerrarConexion() {
        mongoClient.close();
        instancia = null;
    }
}