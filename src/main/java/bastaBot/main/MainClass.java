package bastaBot.main;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

public class MainClass{

    public static void main(String[] args) {

        // Se inicializa el contexto de la API
        ApiContextInitializer.init();
        // Se crea un nuevo Bot API
        final TelegramBotsApi telegramBotsApi = new TelegramBotsApi();

        try {
            // Se registra el bot
            telegramBotsApi.registerBot(new bastaBot.bot.BastaBot());

        } catch (
                TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
