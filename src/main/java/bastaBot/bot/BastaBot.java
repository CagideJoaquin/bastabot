package bastaBot.bot;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import bastaBot.dao.MongoDAO;

import java.util.ArrayList;
import java.util.List;

public class BastaBot extends TelegramLongPollingBot {

    private MongoDAO mongo;

    public BastaBot() {
        this.mongo = new MongoDAO();
        Thread thread = new Thread() {
            public void run() {
                super.run();
                mongo.checkPause();
                mongo.checkLabels();
                try {
                    sleep(3600000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    @Override
    public void onUpdateReceived(Update update) {

        // We check if the update has a message and the message has text
        if (update.hasMessage() && update.getMessage().hasText()) {
            // Set variables
            String message_text = update.getMessage().getText();
            long chat_id = update.getMessage().getChatId();
            SendMessage message = new SendMessage();

            if (message_text.equals("/help") | message_text.equals("/ayuda")) {
                message.setChatId(chat_id)
                        .setText("Bienvenido al BOT del proyecto B.A.S.T.A.\n" +
                                "A continuación se listan los comandos disponibles: \n" +
                                "/ayuda: muestra este mensaje con los comandos disponibles.\n" +
                                "/subscribirse: subscribe al usuario al BastaBot.\n" +
                                "/subscripto: responde SI o NO según si el usuario esta subscrito al BastaBot.\n" +
                                "/comenzar: empieza a recibir mensajes para etiquetar. \n" +
                                "/pause: deja de recibir mensajes para etiquetar, pero no desubscribe al usuario. \n" +
                                "/desubscribirse: deja de recibir mensajes y deja de estar subscrito al BastaBot.\n");

            } else if (message_text.equals("/subscribe") | message_text.equals("/subscribirse")) {
                if (mongo.insertUser(chat_id)) {
                    message.setChatId(chat_id).setText("Subscrito correctamente.");
                } else {
                    message.setChatId(chat_id).setText("Ya te encuentras subscrito al BastaBot.");
                }
            } else if (message_text.equals("/subscribed") | message_text.equals("/subscripto")) {
                if (mongo.subscribed(chat_id)) {
                    message.setChatId(chat_id).setText("Si.");
                } else {
                    message.setChatId(chat_id).setText("No.");
                }
            } else if (message_text.equals("/start") | message_text.equals("/comenzar")) {
                if (mongo.subscribed(chat_id)) {
                    mongo.startUser(chat_id);
                    String tweet = mongo.getMessage(chat_id);
                    if (tweet.equals("")) {
                        message.setChatId(chat_id).setText("No hay más mensajes en la base de datos.\n");
                    } else {
                        message.setChatId(chat_id).setText("¿Es este un mensaje violento?\n" +
                                tweet);
                        setViolentButtons(message);
                    }
                } else {
                    message.setChatId(chat_id).setText("No te encuentras subscrito al BastaBot.\n");
                }
            } else if (message_text.equals("/pause") | message_text.equals("/pausa")) {
                if (mongo.subscribed(chat_id)) {
                    if (mongo.pauseUser(chat_id)) {
                        message.setChatId(chat_id).setText("Te encuentras pausado.\n Introduce /comenzar para empezar a recibir mensajes nuevamente.");
                    } else {
                        message.setChatId(chat_id).setText("Ha ocurrido un problema. Intenta nuevamente más tarde.\n");
                    }
                } else {
                    message.setChatId(chat_id).setText("No te encuentras subscrito al BastaBot.\n");
                }
            } else if (message_text.equals("/unsubscribe") | message_text.equals("/desubscribirse")) {
                if (mongo.subscribed(chat_id)) {
                    if (mongo.unsubscribe(chat_id)) {
                        message.setChatId(chat_id).setText("Te desubscribiste exitosamente.\n");
                    } else {
                        message.setChatId(chat_id).setText("Ha ocurrido un problema, intenta más tarde.\n");
                    }
                } else {
                    message.setChatId(chat_id).setText("No te encuentras subscrito al BastaBot.\n");
                }
            } else if (message_text.equals("Violento")) {
                if (mongo.assignLabel(chat_id, "violento")) {
                    mongo.startUser(chat_id);
                    String tweet = mongo.getMessage(chat_id);
                    if (tweet.equals("")) {
                        message.setChatId(chat_id).setText("No hay más mensajes en la base de datos.\n");
                    } else {
                        message.setChatId(chat_id).setText("¿Qué tipo de mensaje es?\n" +
                                tweet);
                        setViolentButtons(message);
                    }
                } else {
                    message.setChatId(chat_id).setText("No hay mensajes pendientes de etiquetar.\n" +
                            "Presiona /comenzar para empezar a recibir mensajes.");
                }
            } else if (message_text.equals("Tensión")) {
                if (mongo.assignLabel(chat_id, "tension")) {
                    mongo.startUser(chat_id);
                    String tweet = mongo.getMessage(chat_id);
                    if (tweet.equals("")) {
                        message.setChatId(chat_id).setText("No hay más mensajes en la base de datos.\n");
                    } else {
                        message.setChatId(chat_id).setText("¿Qué tipo de mensaje es?\n" +
                                tweet);
                        setViolentButtons(message);
                    }
                } else {
                    message.setChatId(chat_id).setText("No hay mensajes pendientes de etiquetar.\n" +
                            "Presiona /comenzar para empezar a recibir mensajes.");
                }
            } else if (message_text.equals("Carisma")) {
                if (mongo.assignLabel(chat_id, "carisma")) {
                    mongo.startUser(chat_id);
                    String tweet = mongo.getMessage(chat_id);
                    if (tweet.equals("")) {
                        message.setChatId(chat_id).setText("No hay más mensajes en la base de datos.\n");
                    } else {
                        message.setChatId(chat_id).setText("¿Qué tipo de mensaje es?\n" +
                                tweet);
                        setViolentButtons(message);
                    }
                } else {
                    message.setChatId(chat_id).setText("No hay mensajes pendientes de etiquetar.\n" +
                            "Presiona /comenzar para empezar a recibir mensajes.");
                }
            } else if (message_text.equals("Ninguna")) {
                if (mongo.assignLabel(chat_id, "ninguna")) {
                    mongo.startUser(chat_id);
                    String tweet = mongo.getMessage(chat_id);
                    if (tweet.equals("")) {
                        message.setChatId(chat_id).setText("No hay más mensajes en la base de datos.\n");
                    } else {
                        message.setChatId(chat_id).setText("¿Qué tipo de mensaje es?\n" +
                                tweet);
                        setViolentButtons(message);
                    }
                } else {
                    message.setChatId(chat_id).setText("No hay mensajes pendientes de etiquetar.\n" +
                            "Presiona /comenzar para empezar a recibir mensajes.");
                }
            } else {
                message.setChatId(chat_id)
                        .setText("La opción ingresada no es válida. Ingrese una opción válida o escriba" + "\n/ayuda " +
                                "para ver las opciones disponibles.\n");
            }
            try {
                execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        } else if (update.hasMessage() && update.getMessage().hasPhoto()) {
            long chat_id = update.getMessage().getChatId();
            SendMessage message = new SendMessage()
                    .setChatId(chat_id)
                    .setText("No se aceptan fotos como entrada.\n" +
                            "Escriba /ayuda para ver las opciones disponibles.");
            try {
                execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    private synchronized void setViolentButtons(SendMessage sendMessage) {
        // Create a keyboard
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        // Create a list of keyboard rows
        List<KeyboardRow> keyboard = new ArrayList<>();

        // First keyboard row
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        // Add buttons to the first keyboard row
        keyboardFirstRow.add(new KeyboardButton("Violento"));

        // Second keyboard row
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        // Add the buttons to the second keyboard row
        keyboardSecondRow.add(new KeyboardButton("Tensión"));

        // Third keyboard row
        KeyboardRow keyboardThirdRow = new KeyboardRow();
        // Add the buttons to the third keyboard row
        keyboardThirdRow.add(new KeyboardButton("Carisma"));

        // Fourth keyboard row
        KeyboardRow keyboardFourthRow = new KeyboardRow();
        // Add the buttons to the third keyboard row
        keyboardFourthRow.add(new KeyboardButton("Ninguna"));

        // Add all of the keyboard rows to the list
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        keyboard.add(keyboardThirdRow);
        keyboard.add(keyboardFourthRow);
        // and assign this list to our keyboard
        replyKeyboardMarkup.setKeyboard(keyboard);
    }

    private synchronized void setRestartButtons(SendMessage sendMessage) {
        // Create a keyboard
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        // Create a list of keyboard rows
        List<KeyboardRow> keyboard = new ArrayList<>();

        // First keyboard row
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        // Add buttons to the first keyboard row
        keyboardFirstRow.add(new KeyboardButton("/comenzar"));

        // Second keyboard row
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        // Add the buttons to the second keyboard row
        keyboardSecondRow.add(new KeyboardButton("/pausa"));

        // Third keyboard row
        KeyboardRow keyboardThirdRow = new KeyboardRow();
        // Add the buttons to the third keyboard row
        keyboardThirdRow.add(new KeyboardButton("/desubscribirse"));

        // Add all of the keyboard rows to the list
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        keyboard.add(keyboardThirdRow);
        // and assign this list to our keyboard
        replyKeyboardMarkup.setKeyboard(keyboard);
    }

    @Override
    public String getBotUsername() {
        // Se devuelve el nombre que dimos al bot al crearlo con el BotFather
        return "BastaBot";
    }

    @Override
    public String getBotToken() {
        // Se devuelve el token que nos generó el BotFather de nuestro bot
        return "972804760:AAHyT49xeG7C0tSoCbTTRsMrQclVV5_EWLA";
    }
}