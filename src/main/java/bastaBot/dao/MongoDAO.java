package bastaBot.dao;

import bastaBot.connection.MongoConnection;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.Date;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;

public class MongoDAO {

    private static final MongoConnection conexion = MongoConnection.getConnection();

    public boolean insertUser(long chat_id) {
        try {
            MongoCollection<Document> collection = conexion.getDatabase().getCollection("subscribers");
            if (!subscribed(chat_id)) {
                Document doc = new Document("chat_id", chat_id)
                        .append("state", "new");
                collection.insertOne(doc);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean subscribed(long chat_id) {

        try {
            MongoCollection<Document> collection = conexion.getDatabase().getCollection("subscribers");
            long found = collection.count(Document.parse("{chat_id :" + chat_id + "}"));
            return found != 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean unsubscribe(long chat_id) {
        try {
            MongoCollection<Document> collection = conexion.getDatabase().getCollection("subscribers");
            collection.deleteOne(Document.parse("{chat_id :" + chat_id + "}"));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean pauseUser(long chat_id) {
        try {
            MongoCollection<Document> collection = conexion.getDatabase().getCollection("subscribers");
            collection.updateOne(eq("chat_id", chat_id), (set("state", "pause")));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean startUser(long chat_id) {
        try {
            MongoCollection<Document> collection = conexion.getDatabase().getCollection("subscribers");
            collection.updateOne(eq("chat_id", chat_id), (set("state", "ready")));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getMessage(long chat_id) {
        try {
            Document doc = getAssignedMessage(chat_id);
            if (doc != null) {
                BasicDBObject whereQuery = new BasicDBObject();
                whereQuery.append("_id", new ObjectId(doc.getString("id_mensaje")));
                FindIterable<Document> tweet = conexion.getDatabase().getCollection("message").find(whereQuery);
                return tweet.first().getString("body");
            } else {

                FindIterable<Document> findIterable = conexion.getDatabase().getCollection("message")
                        .find();
                for (Document document : findIterable) {
                    if (noAssigned(document, chat_id)) {
                        assignMessage(chat_id, document.getObjectId("_id").toString());
                        return document.getString("body");
                    }
                }
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private Document getAssignedMessage(long chat_id) {
        try {
            BasicDBObject whereQuery = new BasicDBObject();
            whereQuery.append("chat_id", chat_id);
            whereQuery.append("etiqueta", "none");
            FindIterable<Document> doc = conexion.getDatabase().getCollection("etiqueta").find(whereQuery);
            return doc.first();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean assignMessage(long chat_id, String message_id) {
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            conexion.getDatabase().getCollection("etiqueta").insertOne(
                    new Document("id_mensaje", message_id)
                            .append("chat_id", chat_id)
                            .append("etiqueta", "none")
                            .append("fecha", timestamp));

            FindIterable<Document> findIterable = conexion.getDatabase().getCollection("message")
                    .find(eq("_id", new ObjectId(message_id)));

            JSONObject jsonObject = new JSONObject(findIterable.first().toJson());

            for (int i = 1; i <= 10; i++) {
                if (jsonObject.getJSONObject("assigned").get("chat_" + i).equals("")) {
                    conexion.getDatabase().getCollection("message")
                            .findOneAndUpdate(eq("_id", new ObjectId(message_id)), (set("assigned.chat_" + i, Long.toString(chat_id))));
                    return true;
                }
            }

            return false;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean noAssigned(Document doc, long chat_id) {
        try {
            JSONObject jsonObject = new JSONObject(doc.toJson());
            for (int i = 1; i <= 10; i++) {
                if (jsonObject.getJSONObject("assigned").get("chat_" + i).equals(Long.toString(chat_id))) {
                    return false;
                }
            }
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean assignLabel(long chat_id, String label) {
        try {
            Document doc = getAssignedMessage(chat_id);
            if (doc != null) {
                conexion.getDatabase().getCollection("etiqueta").updateOne(
                        eq("_id", doc.getObjectId("_id")), (set("etiqueta", label)));

                if (countLabels(doc.getString("id_mensaje"))) {
                    conexion.getDatabase().getCollection("message").updateOne(
                            eq("_id", new ObjectId(doc.getString("id_mensaje"))), (set("state", "ready")));
                }

                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean countLabels(String message_id) {
        try {
            MongoCollection<Document> collection = conexion.getDatabase().getCollection("etiqueta");
            BasicDBObject whereQuery = new BasicDBObject();
            whereQuery.append("id_mensaje", message_id);
            long found = collection.count(whereQuery);
            return found == 10;
        } catch (
                Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void checkPause() {
        try {
            MongoCollection<Document> collection = conexion.getDatabase().getCollection("etiqueta");
            BasicDBObject whereQuery = new BasicDBObject();
            whereQuery.append("etiqueta", "none");
            FindIterable<Document> doc = collection.find(whereQuery);
            for (Document d : doc) {

                Date tweetDate = (Date) d.get("fecha");
                Timestamp tweetTime = new Timestamp(tweetDate.getTime());

                Date date = new java.util.Date();
                Timestamp actualTime = new Timestamp(date.getTime());

                long milliseconds = actualTime.getTime() - tweetTime.getTime();
                int seconds = (int) milliseconds / 1000;

                int hours = seconds / 3600;
                if (hours > 1) {

                    FindIterable<Document> findIterable = conexion.getDatabase().getCollection("message")
                            .find(eq("_id", new ObjectId(d.getString("id_mensaje"))));

                    JSONObject jsonObject = new JSONObject(findIterable.first().toJson());

                    for (int i = 1; i <= 10; i++) {
                        if (jsonObject.getJSONObject("assigned").get("chat_" + i).equals(Integer.toString((Integer) d.get("chat_id")))) {
                            conexion.getDatabase().getCollection("message")
                                    .findOneAndUpdate(eq("_id", new ObjectId(d.getString("id_mensaje"))), (set("assigned.chat_" + i, "")));
                        }
                    }

                    this.pauseUser(Long.valueOf((Integer) d.get("chat_id")));
                    conexion.getDatabase().getCollection("etiqueta").deleteOne(eq("_id", d.getObjectId("_id")));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkLabels() {

        MongoCollection<Document> messageCollection = conexion.getDatabase().getCollection("message");
        MongoCollection<Document> etiquetaCollection = conexion.getDatabase().getCollection("etiqueta");

        BasicDBObject messageObject = new BasicDBObject();
        messageObject.append("state", "ready");
        FindIterable<Document> messageDocs = messageCollection.find(messageObject);

        for (Document doc : messageDocs) {
            BasicDBObject etiqueteObject = new BasicDBObject();
            etiqueteObject.append("message_id", doc.getObjectId("_id"));
            FindIterable<Document> etiqueteDocs = etiquetaCollection.find(etiqueteObject);

            String label = getMaxLabel(etiqueteDocs);

            conexion.getDatabase().getCollection("label_message")
                    .insertOne(new Document()
                            .append("body", doc.get("body"))
                            .append("label", label));

            messageCollection.deleteOne(doc);
            etiquetaCollection.deleteMany(etiqueteObject);
        }
    }

    private String getMaxLabel(FindIterable<Document> docs) {
        int violento = 0;
        int tension = 0;
        int carisma = 0;
        int ninguna = 0;
        int max = 0;
        String result = "none";
        for (Document doc : docs) {
            if (doc.get("etiqueta").equals("violento")) {
                violento = violento + 1;
            } else if (doc.get("etiqueta").equals("tension")) {
                tension = tension + 1;
            } else if (doc.get("etiqueta").equals("carisma")) {
                carisma = carisma + 1;
            } else if (doc.get("etiqueta").equals("ninguna")) {
                ninguna = ninguna + 1;
            }
        }

        if (violento >= max) {
            max = violento;
            result = "violento";
        }
        if (tension > max) {
            max = tension;
            result = "tension";
        }
        if (carisma > max) {
            max = carisma;
            result = "carisma";
        }
        if (ninguna > max) {
            result = "ninguna";
        }

        return result;
    }
}